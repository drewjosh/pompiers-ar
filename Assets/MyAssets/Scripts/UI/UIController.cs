using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    // UI
    public Button returnToTruckButton;
    SpawnController spawnController;

    // Start is called before the first frame update
    void Start()
    {
        SetReturnToTruckButtonVisibility(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    /**
     * Set visibility of return to truck button
     */
    public void SetReturnToTruckButtonVisibility(bool isShown)
    {
        returnToTruckButton.gameObject.SetActive(isShown);
    }

    /**
     * Set spawn controller from prefab as soon as spawned.
     */
    public void InitSpawnController(SpawnController controller)
    {
        spawnController = controller;
    }

    /**
     * User clicked to return to truck view
     */
    public void OnButtonClickedReturnToTruck()
    {
        spawnController.ReturnToTruck();
    }
}
