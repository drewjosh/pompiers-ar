using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Draw dotted line between two points.
 * 
 * Source (cahnged to Vector3): https://medium.com/@kunaltandon.kt/creating-a-dotted-line-in-unity-ca044d02c3e2
 */
public class DottedLine : MonoBehaviour
{
    // Inspector fields
    public Sprite Dot;

    float Size = 0.01f;
    float Delta = 0.03f;
    int amountOfDots = 10;

    //Static Property with backing field
    private static DottedLine instance;
    public static DottedLine Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<DottedLine>();
            return instance;
        }
    }

    //Utility fields
    List<Vector3> positions = new List<Vector3>();
    List<GameObject> dots = new List<GameObject>();

    // Update is called once per frame
    void FixedUpdate()
    {
        if (positions.Count > 0)
        {
            DestroyAllDots();
            positions.Clear();
        }

    }

    private void DestroyAllDots()
    {
        foreach (var dot in dots)
        {
            Destroy(dot);
        }
        dots.Clear();
    }

    GameObject GetOneDot()
    {
        var gameObject = new GameObject();
        gameObject.transform.localScale = Vector3.one * Size;
        gameObject.transform.parent = transform;

        var sr = gameObject.AddComponent<SpriteRenderer>();
        sr.sprite = Dot;
        return gameObject;
    }

    public void DrawDottedLine(Vector3 start, Vector3 end)
    {
        DestroyAllDots();

        Vector3 point = start;
        Vector3 direction = (end - start).normalized;

        while ((end - start).magnitude > (point - start).magnitude)
        {
            positions.Add(point);
            point += (direction * Delta);
        }

        Render();
    }

    private void Render()
    {
        foreach (var position in positions)
        {
            var g = GetOneDot();
            g.transform.position = position;
            g.transform.rotation = Camera.main.transform.rotation;
            dots.Add(g);
        }
    }

    /**
     * Adjust delta according to y of object
     */
    public void ChangeDelta(float objectY)
    {
        Delta = objectY / amountOfDots;
    }
}