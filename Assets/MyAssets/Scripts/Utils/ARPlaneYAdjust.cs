using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Adjust height of AR planes because occlusion causes flickering of points
 */
public class ARPlaneYAdjust : MonoBehaviour
{
    public float floorOffset = 0.05f;
    float initialY;

    private void Start()
    {
        initialY = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(transform.position.x, initialY + floorOffset, transform.position.z);
    }
}
