using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARRaycastManager))]
public class ARPlacement : MonoBehaviour
{
    public GameObject prefabToSpawn; // prefab of object to spawn
    private GameObject spawnedObject; // spawned object
    private SpawnController spawnController; // controller of spawned object

    private ARRaycastManager arRaycastManager; // manager to get raycast

    float touchStartTime = 0f; // needed to measured delta of hold/pressed
    bool touchStarted = false; // true when touch input started
    float holdingTreshold = 0.5f; // seconds to count as tap and hold

    bool arPlanesShown = true; // is true when planes are shown, default at start is true

    private float initialDistance; // needed to measure toiuch distance for scaling
    private Vector3 initialScale; // used to save initial scale of scaler object


    static List<ARRaycastHit> hits = new List<ARRaycastHit>();

    private void Awake()
    {
        arRaycastManager = GetComponent<ARRaycastManager>();
    }

    bool TryGetTabAndHoldPosition(out Vector2 touchPosition)
    {
        if (Input.touchCount > 0 && touchStarted)
        {
            float delta = Time.time - touchStartTime;
            if (delta > holdingTreshold)
            {
                // touching and holding 
                touchPosition = Input.GetTouch(0).position;
                return true;
            }
        }

        touchPosition = default;
        return false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 1 && !touchStarted)
        {
            // started touching with one finger
            touchStarted = true;
            touchStartTime = Time.time;
        }

        if (Input.touchCount == 0 || Input.touchCount > 1)
        {
            // reset when stopped touching or multi touch
            touchStarted = false;
            touchStartTime = 0f;
        }

        if (Input.touchCount == 0 && spawnController != null && spawnController.isScaling)
        {
            // scaling stoped
            spawnController.isScaling = false;
            spawnController.ResetScalerParent();
        }


        if (spawnController != null && !spawnController.isMovingOnPlane)
        {
            // there is an object and it is not beeing moved horizontally on plane

            HandleMovingUpAndDownActivation();

            HandleRotationActivation();

            HandleScaling();

            if (spawnController.isRotating || spawnController.isMovingUpAndDown)
            {
                // don't do anything else we are rotating or moving up and down
                return;
            }
        }

        HandleMovingOnPlane();
    }

    /**
     * Set visibility of ARPlanes found in scene
     */
    void ShowARPlanes(bool isShown)
    {
        if (arPlanesShown != isShown)
        {
            arPlanesShown = isShown;
            // only update if received other bool
            ARPlane[] planes = GameObject.FindObjectsOfType<ARPlane>();
            foreach (var plane in planes)
            {
                plane.gameObject.GetComponent<ARPlaneMeshVisualizer>().enabled = isShown;
            }
        }

    }

    /**
     * Handles moving the object on an AR plane
     */
    void HandleMovingOnPlane()
    {
        if (!TryGetTabAndHoldPosition(out Vector2 touchPosition))
        {
            // there is no tap and hold position

            if (spawnController != null)
            {
                // we already have an object spawned -> deactivate elements
                spawnController.ShowBaseAndStem(false);
                ShowARPlanes(false);
            }
            return;
        }

        if (spawnedObject != null && Input.touchCount == 1)
        {
            spawnController.ShowBaseAndStem(true);
            ShowARPlanes(true);
        }

        // we have a hold position :)

        if (Input.touchCount == 1 && arRaycastManager.Raycast(touchPosition, hits, TrackableType.PlaneWithinPolygon))
        {
            // touch is on an AR plane
            var hitPose = hits[0].pose;

            if (spawnedObject == null)
            {
                // first time spawning of object
                spawnedObject = Instantiate(prefabToSpawn, hitPose.position, hitPose.rotation);
                spawnController = spawnedObject.GetComponent<SpawnController>();
                spawnController.isMovingOnPlane = true;
                spawnController.SpawnTruck();
                spawnController.ShowBaseAndStem(true);
                ShowARPlanes(true);
            }
            else
            {
                // moving object
                spawnedObject.transform.position = hitPose.position;
                spawnController.ShowBaseAndStem(true);
                ShowARPlanes(true);
                spawnController.isMovingOnPlane = true;
            }
        }
    }

    /**
     * Handles scaling of object.
     * 
     * Source: https://www.youtube.com/watch?v=ISBIu6Jzfk8&ab_channel=MohdHamza
     */
    void HandleScaling()
    {
        if (Input.touchCount == 2 && spawnController != null)
        {
            var touchZero = Input.GetTouch(0);
            var touchOne = Input.GetTouch(1);

            // if any one of touchzero or touchOne is cancelled or maybe ended then do nothing
            if (touchZero.phase == TouchPhase.Ended || touchZero.phase == TouchPhase.Canceled ||
                touchOne.phase == TouchPhase.Ended || touchOne.phase == TouchPhase.Canceled)
            {
                return; // basically do nothing
            }

            if (touchZero.phase == TouchPhase.Began || touchOne.phase == TouchPhase.Began)
            {
                initialDistance = Vector2.Distance(touchZero.position, touchOne.position);
                initialScale = spawnController.scalerParent.transform.localScale;
            }
            else // if touch is moved
            {
                var currentDistance = Vector2.Distance(touchZero.position, touchOne.position);

                //if accidentally touched or pinch movement is very very small
                if (Mathf.Approximately(initialDistance, 0))
                {
                    return; // do nothing if it can be ignored where inital distance is very close to zero
                }

                if (!spawnController.isScaling)
                {
                    spawnController.isScaling = true;
                    spawnController.PrepareForScale();
                }

                var factor = currentDistance / initialDistance;
                spawnController.scalerParent.transform.localScale = initialScale * factor; // scale multiplied by the factor we calculated
            }
        }

    }

    /**
     * Activates up and down movement activation.
     */
    void HandleMovingUpAndDownActivation()
    {
        if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Stationary && !spawnController.isRotating)
        {
            // activate up and down movement, cannot be activated when already is rotating
            Ray ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                float delta = Time.time - touchStartTime;
                if (delta > holdingTreshold && hit.transform.tag == "dragable")
                {
                    // activate up and down
                    spawnController.isMovingUpAndDown = true;
                    spawnController.isRotating = false;
                    return;
                }
            }
        }
    }

    /**
     * Activates rotation.
     */
    void HandleRotationActivation()
    {
        if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Moved && !spawnController.isMovingUpAndDown)
        {
            // activate rotation, cannot be activated when already is moving up and down
            Ray ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag == "dragable")
                {
                    // activate rotation
                    spawnController.isRotating = true;
                    spawnController.isMovingUpAndDown = false;
                    return;
                }
            }
        }

    }
}
