using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARObject : MonoBehaviour
{
    public int objectId; // internal number to identify this object
    public string objectNameTranslationKey; // translation key to name of object
    public float baseOffset = 0.15f; // indications distance from bottom of object to pivot point, used to defined limit so user can place object on floor, note: has to be scaled relative to scale before starting to scale
    public float defaultY = 0.3f; // default value at which height the object should be rendered

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
