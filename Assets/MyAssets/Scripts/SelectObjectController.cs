using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectObjectController : MonoBehaviour
{
    public ARObject objectToShow; // holds GameObject that is shown when clicked
    public SpawnController spawnController;

    // Update is called once per frame
    void Update()
    {
        //always check for touchcount first, before checking array
        if (gameObject.activeSelf && (Input.GetMouseButtonDown(0) || (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)) && !spawnController.IsObjectBeingPlaced())
        {
            Ray raycast;
            if (Input.touchCount == 1)
            {
                raycast = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            }
            else
            {
                raycast = Camera.main.ScreenPointToRay(Input.mousePosition);
            }

            if (Physics.Raycast(raycast, out RaycastHit raycastHit))
            {
                if (raycastHit.collider.name == gameObject.name)
                {
                    Debug.Log("Clicked point: " + gameObject.name);
                    spawnController.SpawnNewObject(objectToShow);
                }
            }
        }
    }
}
