using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Controller for spawn of objects
 * 
 * Rotation tutorial: https://www.mooict.com/unity-3d-tutorial-drag-and-rotate-game-object-with-touch-controls/
 */
public class SpawnController : MonoBehaviour
{
    public DottedLine stem;
    public MeshRenderer baseRenderer;
    MeshRenderer objectRenderer; // holding renderer of object
    ARObject currentObject; // data model of currently rendered object
    public GameObject scalerParent; // parent for scaling, should aways be at base of object
    public GameObject pointsParent; // parent holding all points that can be clicked, only show when not being placed
    public ARObject defaultSpawnObject; // holding the truck object, to rest from object view

    public Material transparentMaterial; // holds transparent material
    Material tempMaterial; // used to temporaily store normal material while transparent
    bool isTransparent = false;

    public bool isRotating = false; // true when rotation mode is on

    public bool isMovingUpAndDown = false; // true when up and down mode is one

    public bool isMovingOnPlane = false; // true when moving on plane

    public bool isScaling = false; // true when object is being scaled

    // match change of numbers with UI visuals
    public float delayedRotationSpeed = 0.3f;
    public float delayedLiftSpeed = 0.0005f;

    // UI controller
    UIController uiController;

    Vector3 initialObjectScale;

    // Update is called once per frame
    void Update()
    {
        if (isRotating || isMovingUpAndDown || isMovingOnPlane)
        {
            HandleARPlacement();
        }
    }

    /**
     * Handles placement of object.
     */
    void HandleARPlacement()
    {
        if (Input.touchCount == 1)
        {
            // operations with one touch: movement on plane, rotation, moving up and down
            Touch screenTouch = Input.GetTouch(0);

            if (screenTouch.phase == TouchPhase.Ended)
            {
                // touch ended -> reset flag, reset object material
                isRotating = false;
                isMovingUpAndDown = false;
                isMovingOnPlane = false;

                if (isTransparent)
                {
                    SwapTransparentMaterial();
                }
            }

            if (isMovingUpAndDown)
            {
                DrawStem();
            }

            if (IsObjectBeingPlaced())
            {
                pointsParent.SetActive(false);
            }
            else
            {
                // only show points when not being placed
                pointsParent.SetActive(true);
            }

            if ((isMovingUpAndDown || isMovingOnPlane) && !isTransparent)
            {
                // swap material for up and down and plane movement
                SwapTransparentMaterial();
            }

            if (screenTouch.phase == TouchPhase.Moved && isRotating)
            {
                // handle rotation on y axis of object with delta of x axis of touch
                transform.Rotate(0f, delayedRotationSpeed * -screenTouch.deltaPosition.x, 0f);
                return;
            }

            if (screenTouch.phase == TouchPhase.Moved && isMovingUpAndDown)
            {
                // handle up and down movement with delta of y axis of touch
                Vector3 localObjectPosition = objectRenderer.gameObject.transform.transform.localPosition;
                float newY = localObjectPosition.y + delayedLiftSpeed * screenTouch.deltaPosition.y;
                if (newY - currentObject.baseOffset >= 0)
                {
                    // object is not below floor
                    objectRenderer.gameObject.transform.localPosition = new Vector3(localObjectPosition.x, localObjectPosition.y + delayedLiftSpeed * screenTouch.deltaPosition.y, localObjectPosition.z);
                }
                return;
            }
        }
    }

    /**
     * Swap object material with transparent material and back
     */
    void SwapTransparentMaterial()
    {
        if (isTransparent)
        {
            objectRenderer.material = tempMaterial;
        }
        else
        {
            tempMaterial = objectRenderer.material;
            objectRenderer.material = transparentMaterial;
        }
        isTransparent = !isTransparent;
    }

    /**
     * Set visibility of base and sten
     */
    public void ShowBaseAndStem(bool isShown)
    {
        if (isShown)
        {
            DrawStem();
        }
        baseRenderer.enabled = isShown;
    }

    /**
     * Draw dotted steam.
     */
    public void DrawStem()
    {
        DottedLine.Instance.ChangeDelta(objectRenderer.transform.localPosition.y);
        DottedLine.Instance.DrawDottedLine(baseRenderer.gameObject.transform.position, objectRenderer.transform.position);
    }

    /**
     * In the beginning was the truck...
     */
    public void SpawnTruck()
    {
        // set up UI controller
        uiController = FindObjectOfType<UIController>();
        uiController.InitSpawnController(this);

        // spawn truck
        currentObject = defaultSpawnObject;
        SpawnNewObject(currentObject);
    }

    /**
     * Handles scaling start, setting position of scaler and setting as parent of object
     */
    public void PrepareForScale()
    {
        Vector3 localScale = currentObject.transform.localScale;
        initialObjectScale = new Vector3(localScale.x, localScale.y, localScale.z);

        // set position of scaler to base of object
        scalerParent.transform.localPosition = new Vector3(currentObject.transform.localPosition.x, currentObject.transform.localPosition.y - currentObject.baseOffset, currentObject.transform.localPosition.z);

        // set object as child of scaler object
        currentObject.transform.SetParent(scalerParent.transform);
    }

    /**
     * Sets object as parent of spawn again
     */
    public void ResetScalerParent()
    {
        // correct base offset according to scale difference
        Vector3 currentScale = currentObject.transform.localScale;
        float factor = currentScale.x / initialObjectScale.x;
        currentObject.baseOffset *= factor;

        // reset parent of object
        currentObject.transform.SetParent(gameObject.transform);

        // reset scale of parent scaler
        scalerParent.transform.localScale = new Vector3(1, 1, 1);
    }

    /**
     * Handles spawning a new object
     */
    public void SpawnNewObject(ARObject newObject)
    {
        Debug.Log("Spawn: " + newObject.objectNameTranslationKey);

        if (newObject.objectId != 0)
        {
            // display truck button if not truck being spawned
            uiController.SetReturnToTruckButtonVisibility(true);
        }

        // hide old object
        currentObject.gameObject.SetActive(false);
        objectRenderer = null;

        // set new object
        currentObject = newObject;

        // get data model for this object
        objectRenderer = currentObject.gameObject.GetComponent<MeshRenderer>();

        // set default y from data model
        currentObject.transform.localPosition = new Vector3(currentObject.transform.localPosition.x, currentObject.defaultY, currentObject.transform.localPosition.z);

        // view new object
        currentObject.gameObject.SetActive(true);
    }

    /**
     * Returns true if object is currently being placed
     */
    public bool IsObjectBeingPlaced()
    {
        return isMovingOnPlane || isRotating || isScaling || isMovingUpAndDown;
    }

    /**
     * User clicked to return to truck view
     */
    public void ReturnToTruck()
    {
        uiController.SetReturnToTruckButtonVisibility(false);
        SpawnNewObject(defaultSpawnObject);
    }
}
