# Firefighter AR

An AR app to discover equipment and vehicles of firefighters

Project start: 3. December 2022

# Vision

As a rookie volunteer firefighter in the French speaking part of Switzerland I'm having a hard time with the huge amount of equipment used on missions. This app should make it fun and easier to learn everything about the vehicles and the equipment of firefigthers.

# Community

Let me know what you think about the project:

- Discord: https://discord.gg/VTnbRvBeWu
- YouTube: https://www.youtube.com/@joshuadrewlow

# Technologies

- Unity: 2021.3.x
- AR Foundation: 5.x

# Target platforms

- iOS
- Android

# 3D models

- Polycam: [@drewjosh](https://poly.cam/@drewjosh)

# Changelog

| Version | Date       | Changes                                                                                                                                                                                                       |
| ------- | ---------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0.2.0   | 21.03.2023 | - version correction<br />- fixed logo for Android<br />- enabled environment occlusion and light source detection<br />- smaller truck model prefab<br />- fixed scaling issues<br />- updated truck texture |
| 0.0.1   | 10.03.2023 | - implemented basic AR placement functionality<br />- added raw fire truck model<br />- added two test items: hose, fire extinguisher<br />- added points to select and view items                            |
